//app/controllers/HorseController.scala

//Horse entity controller

package controllers

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import play.api._
import play.api.mvc._
import play.api.libs.json._
import reactivemongo.bson.BSONObjectID
import repositories.HorseRepository
import models.Horse

class HorseController @Inject()(
    implicit ec: ExecutionContext,
    components: ControllerComponents,
    horsesRepo: HorseRepository) extends AbstractController(components) {

        //Function used to list Horse records from database
        def listHorses = Action.async {
            horsesRepo.list().map { horses =>
                Ok(Json.toJson(horses))
            }
        }

        //Function used to create Horse record inside database
        def createHorse(): Action[JsValue] = Action.async(parse.json) { implicit request =>
            request.session.get("connected_u") match {
                case None => Future.successful(Unauthorized("Unauthorized Access to Service"))
                case Some(connected) =>{
                            request.body
                                .validate[Horse]
                                .map { horse =>
                                    horsesRepo.create(horse).map { response =>
                                        Created
                                    }
                                }
                                .getOrElse(Future.successful(BadRequest("Invalid format")))
                        } 
            }
        }

        //Function used to get Horse record from database
        def readHorse(id: BSONObjectID): Action[AnyContent] = Action.async { implicit request =>
            request.session.get("connected_u") match {
                case Some(connected) => {
                     horsesRepo.read(id).map { maybeHorse =>
                        maybeHorse.map { horse =>
                            Ok(Json.toJson(horse))
                        }.getOrElse(NotFound)
                     }
                } 
                case None => Future.successful(Unauthorized("Unauthorized Access to Service"))
            }  
        }  
        
        //Function used to update Horse record iside database
        def updateHorse(id: BSONObjectID) = Action.async(parse.json) {
            _.body
            .validate[Horse]
            .map { horse =>
                horsesRepo.update(id, horse).map {
                    case Some(horse) => Ok(Json.toJson(horse))
                    case _          => NotFound
                }
            }
            .getOrElse(Future.successful(BadRequest("Invalid format")))
        }

        //Function used to delete Horse record from database
        def deleteHorse(id: BSONObjectID) = Action.async {
            horsesRepo.destroy(id).map {
                case Some(horse) => Ok(Json.toJson(horse))
                case _          => NotFound   
            }
        }
}