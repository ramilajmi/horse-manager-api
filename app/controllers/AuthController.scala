//app/controllers/AuthController.scala

//API Authentification controller

package controllers

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import play.api._
import play.api.mvc._
import play.api.libs.json._
import reactivemongo.bson.BSONObjectID
import repositories.UserRepository
import models.User

@Singleton
class AuthController @Inject()(
    implicit ec: ExecutionContext,
    cc: ControllerComponents,
    usersRepo: UserRepository
)

    extends AbstractController(cc) {
        
        //Function used to log user and get rights to access WS
        def login(): Action[JsValue] = Action.async(parse.json) {
            _.body
            .validate[User]
            .map { user =>
                usersRepo.read(user.username, user.password).map { logged =>
                    if(Json.toJson(logged).as[JsArray].value.size != 0)
                        Ok(Json.toJson(Json.obj("type" -> true, "message" -> "You are logged", "username" -> user.username))).withSession("connected_u" -> user.username)
                    else
                        Ok(Json.toJson(Json.obj("type" -> false, "message" -> "Please check your credentials", "username" -> "null")))
                }
            }
            .getOrElse(Future.successful(BadRequest("Invalid format")))
        }

        //Function used to disconnect user and and destroy session
        def logout = Action { implicit request =>
            Ok("You are disconnected").withNewSession
        }

    }
