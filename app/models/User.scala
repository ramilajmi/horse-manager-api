//app/models/User.scala

package models

import play.api.libs.json.Json
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json._

//User model
case class User(
  id: Option[BSONObjectID],
  username: String,
  password: String
)

object User {
  //Serve this type as JSON
  implicit val userFormat = Json.format[User]
}