//app/models/Horse.scala


package models

import play.api.libs.json._
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json._

//Horse model
case class Horse(
    id: Option[BSONObjectID], 
    name: String, 
    colour: String, 
    speed: Int, 
    breed: String, 
    image: String
)

object Horse {
    //Serve this type as JSON
    implicit val format: OFormat[Horse] = Json.format[Horse]
}