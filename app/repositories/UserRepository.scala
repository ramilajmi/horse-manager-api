//app/repository/UserRepository.scala

//User Repository

package repositories

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import reactivemongo.bson.{BSONDocument, BSONObjectID}
import reactivemongo.api.{ReadPreference, Cursor}
import models.User
import reactivemongo.api.commands.WriteResult

class UserRepository @Inject()(
        implicit ec: ExecutionContext,
        reactiveMongoApi: ReactiveMongoApi
) {
    //Get users collection from mongo
    private def collection: Future[JSONCollection] =
        reactiveMongoApi.database.map(_.collection("users"))

    //Create record in database
    def create(user: User): Future[WriteResult] = 
        collection.flatMap(_.insert(user))

    //Read record from database
    def read(username: String, password: String): Future[Seq[User]] =
        collection.flatMap(
            _.find(BSONDocument("username" -> username, "password" -> password))
              .cursor[User](ReadPreference.primary)
              .collect[Seq](1, Cursor.FailOnError[Seq[User]]())
        )

    //Update record in database
    def update(id: BSONObjectID, user: User): Future[Option
    [User]] = 
        collection.flatMap(
            _.findAndUpdate(
                BSONDocument("_id" -> id),
                BSONDocument(
                    f"$$set" -> BSONDocument(
                        "username" -> user.username,
                        "password" -> user.password
                    )
                ),
                true
            ).map(_.result[User])
        )
    //Delete record from database
    def destroy(id: BSONObjectID): Future[Option[User]] =
        collection.flatMap(
            _.findAndRemove(BSONDocument("_id" -> id)).map(_.result
            [User])
        )
}