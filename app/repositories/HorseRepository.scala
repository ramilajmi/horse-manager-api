//app/repository/HorseRepository.scala

//Horse Repository

package repositories

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import reactivemongo.bson.{BSONDocument, BSONObjectID}
import reactivemongo.api.{ReadPreference, Cursor}
import models.Horse
import reactivemongo.api.commands.WriteResult

class HorseRepository @Inject()(
        implicit ec: ExecutionContext,
        reactiveMongoApi: ReactiveMongoApi
) {
    //Get horses collection from mongo
    private def collection: Future[JSONCollection] =
        reactiveMongoApi.database.map(_.collection("horses"))

    //read all horses records from database
    def list(limit: Int = 100): Future[Seq[Horse]] = 
        collection.flatMap(
            _.find(BSONDocument())
              .cursor[Horse](ReadPreference.primary)
              .collect[Seq](limit, Cursor.FailOnError[Seq[Horse]]())
        )

    //Create record in database
    def create(horse: Horse): Future[WriteResult] = 
        collection.flatMap(_.insert(horse))

    //Read record from database
    def read(id: BSONObjectID): Future[Option[Horse]] =
        collection.flatMap(_.find(BSONDocument("_id" -> id)).one
        [Horse])
    
    //Update record in database
    def update(id: BSONObjectID, horse: Horse): Future[Option
    [Horse]] = 
        collection.flatMap(
            _.findAndUpdate(
                BSONDocument("_id" -> id),
                BSONDocument(
                    f"$$set" -> BSONDocument(
                        "name" -> horse.name,
                        "colour" -> horse.colour,
                        "speed" -> horse.speed,
                        "breed" -> horse.breed,
                        "image" -> horse.image
                    )
                ),
                true
            ).map(_.result[Horse])
        )

    //Delete record from database
    def destroy(id: BSONObjectID): Future[Option[Horse]] =
        collection.flatMap(
            _.findAndRemove(BSONDocument("_id" -> id)).map(_.result
            [Horse])
        )
}