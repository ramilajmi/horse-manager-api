name := """horse-manager-api"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.6"

resolvers ++= Seq("snapshots", "releases").map(Resolver.sonatypeRepo)

libraryDependencies ++= Seq(
    "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
    "org.reactivemongo" %% "play2-reactivemongo" % "0.20.13-play27",
    guice
)

import play.sbt.routes.RoutesKeys

RoutesKeys.routesImport += "play.modules.reactivemongo.PathBindables._"
